package com.silvioapps.malvelapp.models.characters;

import com.silvioapps.malvelapp.models.RealmListParcelConverter;

import org.parceler.Parcels;

public class UrlsItemListParcelConverter extends RealmListParcelConverter<UrlsItem> {
    @Override
    public void itemToParcel(UrlsItem input, android.os.Parcel parcel) {
        parcel.writeParcelable(Parcels.wrap(input), 0);
    }

    @Override
    public UrlsItem itemFromParcel(android.os.Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(UrlsItem.class.getClassLoader()));
    }
}
