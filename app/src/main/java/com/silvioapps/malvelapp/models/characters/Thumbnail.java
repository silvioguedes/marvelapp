package com.silvioapps.malvelapp.models.characters;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.ThumbnailRealmProxy;

@Parcel(implementations = {ThumbnailRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Thumbnail.class})
public class Thumbnail extends RealmObject {

	@SerializedName("path")
	public String path;

	@SerializedName("extension")
	public String extension;

	public void setPath(String path){
		this.path = path;
	}

	public String getPath(){
		return path;
	}

	public void setExtension(String extension){
		this.extension = extension;
	}

	public String getExtension(){
		return extension;
	}
}