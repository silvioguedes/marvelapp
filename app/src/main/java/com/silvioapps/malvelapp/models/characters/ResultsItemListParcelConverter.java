package com.silvioapps.malvelapp.models.characters;

import com.silvioapps.malvelapp.models.RealmListParcelConverter;

import org.parceler.Parcels;

public class ResultsItemListParcelConverter extends RealmListParcelConverter<ResultsItem> {
    @Override
    public void itemToParcel(ResultsItem input, android.os.Parcel parcel) {
        parcel.writeParcelable(Parcels.wrap(input), 0);
    }

    @Override
    public ResultsItem itemFromParcel(android.os.Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(ResultsItem.class.getClassLoader()));
    }
}
