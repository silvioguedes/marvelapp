package com.silvioapps.malvelapp.models.characters;

import com.silvioapps.malvelapp.models.RealmListParcelConverter;

import org.parceler.Parcels;

public class ItemsItemListParcelConverter extends RealmListParcelConverter<ItemsItem> {
    @Override
    public void itemToParcel(ItemsItem input, android.os.Parcel parcel) {
        parcel.writeParcelable(Parcels.wrap(input), 0);
    }

    @Override
    public ItemsItem itemFromParcel(android.os.Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(ItemsItem.class.getClassLoader()));
    }
}
