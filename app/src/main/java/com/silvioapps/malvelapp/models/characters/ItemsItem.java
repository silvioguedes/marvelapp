package com.silvioapps.malvelapp.models.characters;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import io.realm.RealmObject;
import io.realm.ItemsItemRealmProxy;

@Parcel(implementations = {ItemsItemRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {ItemsItem.class})
public class ItemsItem extends RealmObject {

	@SerializedName("name")
	public String name;

	@SerializedName("resourceURI")
	public String resourceURI;

	@SerializedName("type")
	public String type;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setResourceURI(String resourceURI){
		this.resourceURI = resourceURI;
	}

	public String getResourceURI(){
		return resourceURI;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}
}