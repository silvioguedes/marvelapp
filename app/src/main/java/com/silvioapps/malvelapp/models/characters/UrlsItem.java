package com.silvioapps.malvelapp.models.characters;

import com.google.gson.annotations.SerializedName;
import org.parceler.Parcel;
import io.realm.RealmObject;
import io.realm.UrlsItemRealmProxy;

@Parcel(implementations = {UrlsItemRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {UrlsItem.class})
public class UrlsItem extends RealmObject {

	@SerializedName("type")
	public String type;

	@SerializedName("url")
	public String url;

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}
}