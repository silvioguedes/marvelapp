package com.silvioapps.malvelapp.models.characters;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.DataRealmProxy;

@Parcel(implementations = {DataRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Data.class})
public class Data extends RealmObject {

	@SerializedName("total")
	public int total;

	@SerializedName("offset")
	public int offset;

	@SerializedName("limit")
	public int limit;

	@SerializedName("count")
	public int count;

	@SerializedName("results")
	public RealmList<ResultsItem> results;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setOffset(int offset){
		this.offset = offset;
	}

	public int getOffset(){
		return offset;
	}

	public void setLimit(int limit){
		this.limit = limit;
	}

	public int getLimit(){
		return limit;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	@ParcelPropertyConverter(ResultsItemListParcelConverter.class)
	public void setResults(RealmList<ResultsItem> results){
		this.results = results;
	}

	public RealmList<ResultsItem> getResults(){
		return results;
	}
}