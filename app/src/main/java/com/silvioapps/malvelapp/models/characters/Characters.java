package com.silvioapps.malvelapp.models.characters;

import com.google.gson.annotations.SerializedName;
import com.silvioapps.malvelapp.constants.Constants;

import org.parceler.Parcel;

import io.realm.RealmObject;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import io.realm.CharactersRealmProxy;

@Parcel(implementations = {CharactersRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Characters.class})
public class Characters extends RealmObject {

	@SerializedName("copyright")
	public String copyright;

	@SerializedName("code")
	public int code;

	@SerializedName("data")
	public Data data;

	@SerializedName("attributionHTML")
	public String attributionHTML;

	@SerializedName("attributionText")
	public String attributionText;

	@SerializedName("etag")
	public String etag;

	@SerializedName("status")
	public String status;

	public interface API {
		@Headers({"Accept: application/json"})
        @GET(Constants.CHARACTERS_LIST_URL)
        Call<Characters> get(@Query("limit") int limit, @Query("offset") int offset, @Query("ts") long ts, @Query("apikey") String apikey, @Query("hash") String hash);
	}

	public void setCopyright(String copyright){
		this.copyright = copyright;
	}

	public String getCopyright(){
		return copyright;
	}

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setData(Data data){
		this.data = data;
	}

	public Data getData(){
		return data;
	}

	public void setAttributionHTML(String attributionHTML){
		this.attributionHTML = attributionHTML;
	}

	public String getAttributionHTML(){
		return attributionHTML;
	}

	public void setAttributionText(String attributionText){
		this.attributionText = attributionText;
	}

	public String getAttributionText(){
		return attributionText;
	}

	public void setEtag(String etag){
		this.etag = etag;
	}

	public String getEtag(){
		return etag;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}