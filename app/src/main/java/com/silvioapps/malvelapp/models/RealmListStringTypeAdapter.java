package com.silvioapps.malvelapp.models;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import io.realm.RealmList;

public class RealmListStringTypeAdapter extends TypeAdapter<RealmList<RealmString>> {

    @Override
    public void write(JsonWriter out, RealmList<RealmString> value) throws IOException {
        // Ignore
    }

    @Override
    public RealmList<RealmString> read(JsonReader in) throws IOException {
        RealmList<RealmString> list = new RealmList<>();
        in.beginArray();
        while (in.hasNext()) {
            list.add(new RealmString(in.nextString()));
        }
        in.endArray();
        return list;
    }
}
