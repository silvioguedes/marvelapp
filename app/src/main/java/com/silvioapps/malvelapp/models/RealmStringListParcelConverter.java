package com.silvioapps.malvelapp.models;

import org.parceler.Parcels;

public class RealmStringListParcelConverter extends RealmListParcelConverter<RealmString> {
    @Override
    public void itemToParcel(RealmString input, android.os.Parcel parcel) {
        parcel.writeParcelable(Parcels.wrap(input), 0);
    }

    @Override
    public RealmString itemFromParcel(android.os.Parcel parcel) {
        return Parcels.unwrap(parcel.readParcelable(RealmString.class.getClassLoader()));
    }
}
