package com.silvioapps.malvelapp.models.characters;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.SeriesRealmProxy;

@Parcel(implementations = {SeriesRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {Series.class})
public class Series extends RealmObject {

	@SerializedName("collectionURI")
	public String collectionURI;

	@SerializedName("available")
	public int available;

	@SerializedName("returned")
	public int returned;

	@SerializedName("items")
	public RealmList<ItemsItem> items;

	public void setCollectionURI(String collectionURI){
		this.collectionURI = collectionURI;
	}

	public String getCollectionURI(){
		return collectionURI;
	}

	public void setAvailable(int available){
		this.available = available;
	}

	public int getAvailable(){
		return available;
	}

	public void setReturned(int returned){
		this.returned = returned;
	}

	public int getReturned(){
		return returned;
	}

	@ParcelPropertyConverter(ItemsItemListParcelConverter.class)
	public void setItems(RealmList<ItemsItem> items){
		this.items = items;
	}

	public RealmList<ItemsItem> getItems(){
		return items;
	}
}