package com.silvioapps.malvelapp.models.characters;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.ResultsItemRealmProxy;

@Parcel(implementations = {ResultsItemRealmProxy.class}, value = Parcel.Serialization.BEAN, analyze = {ResultsItem.class})
public class ResultsItem extends RealmObject {

	@SerializedName("thumbnail")
	public Thumbnail thumbnail;

	@SerializedName("urls")
	public RealmList<UrlsItem> urls;

	@SerializedName("stories")
	public Stories stories;

	@SerializedName("series")
	public Series series;

	@SerializedName("comics")
	public Comics comics;

	@SerializedName("name")
	public String name;

	@SerializedName("description")
	public String description;

	@SerializedName("modified")
	public String modified;

	@SerializedName("id")
	public int id;

	@SerializedName("resourceURI")
	public String resourceURI;

	@SerializedName("events")
	public Events events;

    @SerializedName("favorited")
	public boolean favorited = false;

	public void setThumbnail(Thumbnail thumbnail){
		this.thumbnail = thumbnail;
	}

	public Thumbnail getThumbnail(){
		return thumbnail;
	}

	@ParcelPropertyConverter(UrlsItemListParcelConverter.class)
	public void setUrls(RealmList<UrlsItem> urls){
		this.urls = urls;
	}

	public RealmList<UrlsItem> getUrls(){
		return urls;
	}

	public void setStories(Stories stories){
		this.stories = stories;
	}

	public Stories getStories(){
		return stories;
	}

	public void setSeries(Series series){
		this.series = series;
	}

	public Series getSeries(){
		return series;
	}

	public void setComics(Comics comics){
		this.comics = comics;
	}

	public Comics getComics(){
		return comics;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setModified(String modified){
		this.modified = modified;
	}

	public String getModified(){
		return modified;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setResourceURI(String resourceURI){
		this.resourceURI = resourceURI;
	}

	public String getResourceURI(){
		return resourceURI;
	}

	public void setEvents(Events events){
		this.events = events;
	}

	public Events getEvents(){
		return events;
	}

	public boolean isFavorited() {
		return favorited;
	}

	public void setFavorited(boolean favorited) {
		this.favorited = favorited;
	}
}