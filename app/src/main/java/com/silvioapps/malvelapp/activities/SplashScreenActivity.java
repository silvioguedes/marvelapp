package com.silvioapps.malvelapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.silvioapps.malvelapp.constants.Constants;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null) {
            (new Handler()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    showMainActivity();

                    SplashScreenActivity.this.finish();
                }
            }, Constants.SPLASH_SCREEN_SLEEP);
        }
    }

    protected void showMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
