package com.silvioapps.malvelapp.activities;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.fragments.FavoritesListFragment;
import com.silvioapps.malvelapp.utils.Utils;

public class FavoritesActivity extends CustomActivity {
    private FavoritesListFragment favoritesListFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbarSettings();

        if(savedInstanceState == null) {
            attachFragment();
        }
    }

    protected void toolbarSettings(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        if(toolbar != null) {
            Utils.removeToolbarSidePaddings(toolbar);
            setSupportActionBar(toolbar);
            Utils.setStatusBarColor(this, (View)toolbar.getParent(), ResourcesCompat.getColor(getResources(), R.color.colorPrimaryDark, null), false);
        }

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);

            View actionBarLayout = getLayoutInflater().inflate(R.layout.action_bar_layout, null);
            if(actionBarLayout != null) {
                actionBar.setCustomView(actionBarLayout);

                TextView titleTextView = (TextView)actionBarLayout.findViewById(R.id.titleTextView);
                titleTextView.setText(R.string.favorites);
            }
        }
    }

    protected void attachFragment(){
        favoritesListFragment = new FavoritesListFragment();
        attachFragment(R.id.frameLayout, favoritesListFragment);
    }
}
