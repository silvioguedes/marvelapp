package com.silvioapps.malvelapp.activities;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.ImageView;

import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.constants.Constants;
import com.silvioapps.malvelapp.database.DatabaseHandler;
import com.silvioapps.malvelapp.databinding.ActivityCharacterInfoBinding;
import com.silvioapps.malvelapp.databinding.CharactersListBaseObservable;
import com.silvioapps.malvelapp.fragments.CharacterInfoFragment;
import com.silvioapps.malvelapp.receivers.MessageEvent;
import com.silvioapps.malvelapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

public class CharacterInfoActivity extends CustomActivity {
    private CharactersListBaseObservable characterInfoBaseObservable = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent() != null && getIntent().hasExtra(Constants.CHARACTER_INFO)) {
            Parcelable parcelable = getIntent().getBundleExtra(Constants.CHARACTER_INFO).getParcelable(Constants.CHARACTER_INFO_PARCELABLE);

            if(parcelable != null) {
                characterInfoBaseObservable = Parcels.unwrap(parcelable);

                ActivityCharacterInfoBinding activityCharacterInfoBinding = DataBindingUtil.setContentView(this,
                        R.layout.activity_character_info);
                activityCharacterInfoBinding.setList(characterInfoBaseObservable);
            }
        }

        toolbarSettings();

        if(savedInstanceState == null) {
            attachFragment(characterInfoBaseObservable);
        }
    }

    protected void onDestroy(){
        super.onDestroy();

        sendMessageToList();
    }

    protected void toolbarSettings(){
        View actionBarLayout = findViewById(R.id.characterInfoActionBarLayout);
        if (actionBarLayout != null) {
            Utils.setStatusBarColor(this, actionBarLayout, ResourcesCompat.getColor(getResources(), R.color.colorPrimaryDark, null), false);

            final ImageView leftImageView = (ImageView)actionBarLayout.findViewById(R.id.leftImageView);
            leftImageView.setVisibility(View.VISIBLE);
            leftImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onFavorite();
                }
            });
        }
    }

    protected void onFavorite(){
        if(characterInfoBaseObservable != null){
            boolean favorited = !characterInfoBaseObservable.isFavorited();
            characterInfoBaseObservable.setFavorited(favorited);
        }
    }

    protected void sendMessageToList(){
        if(characterInfoBaseObservable != null) {
            DatabaseHandler.saveFavorite(characterInfoBaseObservable.getId(), characterInfoBaseObservable.isFavorited());

            Intent intent = new Intent(this, FavoritesActivity.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.CHARACTER_INFO_PARCELABLE, Parcels.wrap(characterInfoBaseObservable));
            intent.putExtra(Constants.CHARACTER_INFO, bundle);

            EventBus.getDefault().postSticky(new MessageEvent(intent));

            DatabaseHandler.closeRealm();
        }
    }

    protected void attachFragment(CharactersListBaseObservable characterInfoBaseObservable){
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.CHARACTER_INFO_PARCELABLE, Parcels.wrap(characterInfoBaseObservable));

        CharacterInfoFragment fragment = new CharacterInfoFragment();
        fragment.setArguments(bundle);

        attachFragment(R.id.frameLayout, fragment);
    }
}
