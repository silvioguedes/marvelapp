package com.silvioapps.malvelapp.activities;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.constants.Constants;
import com.silvioapps.malvelapp.fragments.CharactersListFragment;
import com.silvioapps.malvelapp.utils.Utils;
import com.squareup.picasso.Picasso;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

public class MainActivity extends CustomActivity {
    private DrawerLayout drawerLayout = null;
    private CharactersListFragment charactersListFragment = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbarSettings();

        menuSettings();

        if(savedInstanceState == null) {
            attachFragment();
        }
    }

    protected void toolbarSettings(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolBar);
        if(toolbar != null) {
            Utils.removeToolbarSidePaddings(toolbar);
            setSupportActionBar(toolbar);
            Utils.setStatusBarColor(this, (View)toolbar.getParent(), ResourcesCompat.getColor(getResources(), R.color.colorPrimaryDark, null), false);
        }

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);

            View actionBarLayout = getLayoutInflater().inflate(R.layout.action_bar_layout, null);
            if(actionBarLayout != null) {
                actionBar.setCustomView(actionBarLayout);

                ImageView leftImageView = (ImageView)actionBarLayout.findViewById(R.id.leftImageView);
                leftImageView.setVisibility(View.VISIBLE);
                leftImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onMenu();
                    }
                });
                Picasso.with(this)
                        .load(R.drawable.ic_menu)
                        .into(leftImageView);

                TextView titleTextView = (TextView)actionBarLayout.findViewById(R.id.titleTextView);
                titleTextView.setVisibility(View.VISIBLE);
                titleTextView.setText(R.string.app_name);
            }
        }
    }

    protected void menuSettings(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigationView);

        Utils.setFontNavigationView(navigationView, Constants.FONT_NORMAL_PATH);
        if(navigationView != null) {
            View headerView = navigationView.getHeaderView(0);

            if (headerView != null) {
                RoundedImageView roundedImageView = (RoundedImageView) headerView.findViewById(R.id.roundedImageView);

                Picasso.with(this)
                        .load(R.mipmap.ic_launcher)
                        .into(roundedImageView);

                Point screenSize = Utils.getScreenSize(this);
                int size = screenSize.y / Constants.ROUNDED_IMAGE_SIZE_PERCENT;

                if (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT) {
                    size = screenSize.x / Constants.ROUNDED_IMAGE_SIZE_PERCENT;
                }

                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size, size);
                layoutParams.gravity = Gravity.START;
                layoutParams.leftMargin = (int)getResources().getDimension(R.dimen.activity_horizontal_margin);
                layoutParams.topMargin = (int)getResources().getDimension(R.dimen.activity_vertical_margin) +
                        Utils.getStatusBarHeight(this);

                roundedImageView.setLayoutParams(layoutParams);
            }

            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    if (drawerLayout != null) {
                        drawerLayout.closeDrawers();
                    }

                    switch (menuItem.getItemId()) {
                        case R.id.favorites:
                            onFavorites();
                        default:
                            return false;
                    }
                }
            });
        }
    }

    protected void onMenu(){
        if(drawerLayout != null) {
            drawerLayout.openDrawer(Gravity.START);
        }
    }

    protected void onFavorites(){
        Intent intent = new Intent(this, FavoritesActivity.class);
        startActivity(intent);
    }

    protected void attachFragment(){
        charactersListFragment = new CharactersListFragment();
        attachFragment(R.id.frameLayout, charactersListFragment);
    }
}
