package com.silvioapps.malvelapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;

import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.database.DatabaseHandler;
import com.silvioapps.malvelapp.receivers.MessageEvent;
import com.silvioapps.malvelapp.utils.Utils;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CustomActivity extends AppCompatActivity{
    private int statusBarColor = R.color.colorPrimaryDark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.setStatusBarColor(this, ResourcesCompat.getColor(getResources(), statusBarColor, null), false);
    }

    @Override
    protected void onStart(){
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume(){
        super.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onStop(){
        EventBus.getDefault().unregister(this);

        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();

        DatabaseHandler.destroyInstance();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected void attachFragment(int resId, Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentById = getSupportFragmentManager().findFragmentById(resId);
        if(fragmentById == null && !isFinishing()){
            fragmentTransaction.add(resId, fragment);
            fragmentTransaction.commit();
        }
    }

    protected void replaceFragment(int resId, Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if(!isFinishing()){
            fragmentTransaction.replace(resId, fragment);
            fragmentTransaction.commit();
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if(event != null) {
            onReceiveMessage(event.getIntent());
            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    public void onReceiveMessage(Intent intent){}

    public void setStatusBarColor(int intRes){
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            if(intRes != android.R.color.transparent){
                statusBarColor = intRes;
            }
        }
        else{
            statusBarColor = intRes;
        }
    }

    public int getStatusBarColor(){
        return statusBarColor;
    }
}
