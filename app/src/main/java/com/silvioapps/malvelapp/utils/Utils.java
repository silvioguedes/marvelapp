package com.silvioapps.malvelapp.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class Utils {
    public static boolean isConnectionAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()
                    && netInfo.isConnectedOrConnecting()
                    && netInfo.isAvailable()) {
                    return true;
            }
        }
        return false;
    }

    public static void removeToolbarSidePaddings(Toolbar toolbar) {
        if (toolbar != null) {
            toolbar.setContentInsetsAbsolute(0, 0);
            toolbar.setPadding(0, 0, 0, 0);
        }
    }

    public static void showStatusBarWithouDim(Activity activity, Dialog dialog){
        if(activity != null & dialog != null) {
            Window window = activity.getWindow();
            if(window != null) {
                final int statusBarHeight = getStatusBarHeight(activity);
                float dimAmount = 0.5f;
                if (dialog.getWindow() != null) {
                    dimAmount = dialog.getWindow().getAttributes().dimAmount;
                    dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
                }

                if (window.getDecorView() != null) {
                    final ViewGroup rootView = (ViewGroup)window.getDecorView().getRootView();
                    if (rootView != null) {
                        applyDim(rootView, 0, statusBarHeight, dimAmount);
                    }

                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            clearDim(rootView);
                        }
                    });
                }
            }
        }
    }

    public static void applyDim(@NonNull ViewGroup parent, int x, int y, float dimAmount){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            Drawable dim = new ColorDrawable(Color.BLACK);
            dim.setBounds(x, y, parent.getWidth(), parent.getHeight());
            dim.setAlpha((int) (255 * dimAmount));

            ViewGroupOverlay overlay = parent.getOverlay();
            overlay.add(dim);
        }
    }

    public static void clearDim(@NonNull ViewGroup parent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            ViewGroupOverlay overlay = parent.getOverlay();
            overlay.clear();
        }
    }

    public static void setStatusBarColor(Activity activity, int color, boolean translucent){
        if(activity != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Window window = activity.getWindow();
                if (translucent) {
                    window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        window.setFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS, WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    } else {
                        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    }
                }

                final int statusBarHeight = getStatusBarHeight(activity);

                View view = new View(activity);
                view.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                view.getLayoutParams().height = statusBarHeight;
                view.setBackgroundColor(color);
                ((ViewGroup) window.getDecorView()).addView(view);
            }
        }
    }

    public static void setStatusBarColor(Activity activity, View toolBar, int color, boolean translucent){
        if(activity != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Window window = activity.getWindow();
                if (translucent) {
                    window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        window.setFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS, WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    } else {
                        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                    }
                }

                final int statusBarHeight = getStatusBarHeight(activity);
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
                    if (toolBar != null) {
                        setMargin(toolBar, 0, statusBarHeight, 0, 0);
                    }
                }

                View view = new View(activity);
                view.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                view.getLayoutParams().height = statusBarHeight;
                view.setBackgroundColor(color);
                ((ViewGroup) window.getDecorView()).addView(view);
            }
        }
    }

    public static void setMargin(View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            layoutParams.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static String certifyNotNullOrEmptyUrl(String url){
        if(url == null || url.equals("")){
            return "null";
        }

        return url;
    }

    public static void setFontNavigationView(NavigationView navigationView, String fontPath) {
        if(navigationView != null) {
            Context context = navigationView.getContext();
            Menu menu = navigationView.getMenu();

            if(menu != null) {
                for (int i = 0; i < menu.size(); i++) {
                    MenuItem menuItem = menu.getItem(i);

                    if(menuItem != null) {
                        SubMenu subMenu = menuItem.getSubMenu();

                        if (subMenu != null && subMenu.size() > 0) {
                            for (int j = 0; j < subMenu.size(); j++) {
                                MenuItem subMenuItem = subMenu.getItem(j);
                                applyFontToMenuItem(context, subMenuItem, fontPath);
                            }
                        }

                        applyFontToMenuItem(context, menuItem, fontPath);
                    }
                }
            }
        }
    }

    public static void applyFontToMenuItem(Context context, MenuItem menuItem, String fontPath) {
        if(context != null) {
            Typeface font = Typeface.createFromAsset(context.getAssets(), fontPath);

            if(font != null && menuItem != null && menuItem.getTitle() != null) {
                SpannableString spannableString = new SpannableString(menuItem.getTitle());

                if(spannableString != null) {
                    spannableString.setSpan(new CustomTypefaceSpan("", font), 0, spannableString.length(),
                            Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                    menuItem.setTitle(spannableString);
                }
            }
        }
    }

    public static Point getScreenSize(Context context) {
        if (context != null) {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;

            return new Point(width, height);
        }

        return null;
    }

    public static boolean isAfterKitkat(){
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            return true;
        }

        return false;
    }

    //***********************

    public static String MD5(String md5) {
        if(md5 != null) {
            try {
                java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
                byte[] array = md.digest(md5.getBytes());
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < array.length; ++i) {
                    sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
                }
                return sb.toString();
            } catch (java.security.NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}
