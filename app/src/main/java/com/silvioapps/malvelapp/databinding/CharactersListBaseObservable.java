package com.silvioapps.malvelapp.databinding;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.silvioapps.malvelapp.BR;

import org.parceler.Parcel;

@Parcel
public class CharactersListBaseObservable extends BaseObservable{
    public int id = -1;
    public String name = null;
    public String description = null;
    public String image = null;
    public boolean showLoading = false;
    public boolean isFavorited = false;

    public CharactersListBaseObservable(){}

    public CharactersListBaseObservable(int id, String name, String description, String image) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
        notifyPropertyChanged(BR.name);
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    public void setDescription(String value) {
        this.description = value;
        notifyPropertyChanged(BR.description);
    }

    @Bindable
    public String getImage() {
        return image;
    }

    public void setImage(String value) {
        this.image = value;
        notifyPropertyChanged(BR.image);
    }

    @Bindable
    public boolean isShowLoading() {
        return showLoading;
    }

    public void setShowLoading(boolean showLoading) {
        this.showLoading = showLoading;
        notifyPropertyChanged(BR.showLoading);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Bindable
    public boolean isFavorited() {
        return isFavorited;
    }

    public void setFavorited(boolean value) {
        this.isFavorited = value;
        notifyPropertyChanged(BR.favorited);
    }
}
