package com.silvioapps.malvelapp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.malvelapp.R;

public class LoadingDialogFragment extends CustomDialogFragment {
    private static LoadingDialogFragment instance = null;

    public static LoadingDialogFragment getInstance(){
        if(instance == null){
            instance = new LoadingDialogFragment();
        }

        return instance;
    }

    public static void destroyInstance(){
        instance = null;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = null;

        boolean canceledOnTouchOutside = false;
        boolean transparent = false;
        boolean noBackground = false;
        boolean showStatusBarWithoutDim = true;
        boolean hideTitle = true;
        if(getArguments() != null){
            canceledOnTouchOutside = getArguments().getBoolean(CANCELED_ON_TOUCH_OUTSIDE, canceledOnTouchOutside);
            transparent = getArguments().getBoolean(TRANSPARENT, transparent);
            noBackground = getArguments().getBoolean(NO_BACKGROUND, noBackground);
            showStatusBarWithoutDim = getArguments().getBoolean(SHOW_STATUS_BAR_WITHOUT_DIM, showStatusBarWithoutDim);
            hideTitle = getArguments().getBoolean(HIDE_TITLE, hideTitle);
        }

        if(getActivity() != null) {
            dialog = new Dialog(getActivity());

            if(hideTitle) {
                hideTitle(dialog);
            }

            if (transparent) {
                setTransparent(dialog);
            }

            if (noBackground) {
                setNoBackground(dialog);
            }

            if(showStatusBarWithoutDim) {
                showStatusBarWithouDim(dialog);
            }

            dialog.setCanceledOnTouchOutside(canceledOnTouchOutside);
        }

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = layoutInflater.inflate(R.layout.dialog_fragment_loading, viewGroup, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    public void showLoadingFragment(Fragment fragment){
        if(fragment != null && fragment.getActivity() != null) {
            FragmentManager fragmentManager = fragment.getActivity().getSupportFragmentManager();
            Fragment fragmentFound = fragmentManager.findFragmentByTag(LoadingDialogFragment.class.getName());
            if (fragmentFound == null && !fragment.getActivity().isFinishing()) {
                settings(true, false, true, true, false);
                show(fragment);
            }
        }
    }
}
