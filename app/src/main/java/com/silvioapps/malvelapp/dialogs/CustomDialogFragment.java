package com.silvioapps.malvelapp.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.silvioapps.malvelapp.utils.Utils;

public class CustomDialogFragment extends DialogFragment {
    public static final String CANCELED_ON_TOUCH_OUTSIDE = "CANCELED_ON_TOUCH_OUTSIDE";
    public static final String TRANSPARENT = "TRANSPARENT";
    public static final String NO_BACKGROUND = "NO_BACKGROUND";
    public static final String SHOW_STATUS_BAR_WITHOUT_DIM = "SHOW_STATUS_BAR_WITHOUT_DIM";
    public static final String HIDE_TITLE = "HIDE_TITLE";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public void onDestroyView(){
        Dialog dialog = getDialog();
        // handles https://code.google.com/p/android/issues/detail?id=17423
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }

        super.onDestroyView();
    }

    @Override
    public void onCancel(DialogInterface dialogInterface){
        cancel();
    }

    public void cancel(){
        if(getActivity() != null && !getActivity().isFinishing()) {
            try {
                dismiss();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public void show(AppCompatActivity appCompatActivity){
        if (appCompatActivity != null) {
            FragmentManager fragmentManager = appCompatActivity.getSupportFragmentManager();

            Fragment fragment = fragmentManager.findFragmentByTag(getClass().getName());
            if(fragment == null && !appCompatActivity.isFinishing()) {
                show(fragmentManager, getClass().getName());
            }
        }
    }

    public void show(Fragment parent){
        if (parent != null) {
            FragmentManager fragmentManager = parent.getChildFragmentManager();

            DialogFragment dialogFragment = (DialogFragment)fragmentManager.findFragmentByTag(getClass().getName());
            if(parent.getActivity() != null && !parent.getActivity().isFinishing()) {
                if (dialogFragment == null) {
                    show(fragmentManager, getClass().getName());
                } else {
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.remove(dialogFragment);
                    fragmentTransaction.add(dialogFragment, getClass().getName());
                    fragmentTransaction.commit();
                }
            }
        }
    }

    public void settings(boolean hideTitle, boolean canceledOnTouchOutside, boolean noBackground,
                         boolean transparent, boolean showStatusBarWithouDim){
        Bundle bundle = new Bundle();
        bundle.putBoolean(HIDE_TITLE, hideTitle);
        bundle.putBoolean(CANCELED_ON_TOUCH_OUTSIDE, canceledOnTouchOutside);
        bundle.putBoolean(NO_BACKGROUND, noBackground);
        bundle.putBoolean(TRANSPARENT, transparent);
        bundle.putBoolean(SHOW_STATUS_BAR_WITHOUT_DIM, showStatusBarWithouDim);

        setArguments(bundle);
    }

    protected void hideTitle(Dialog dialog){
        if(dialog.getWindow() != null) {
            if (!dialog.getWindow().hasFeature(Window.FEATURE_NO_TITLE)) {
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            }
        }
    }

    protected void setNoBackground(Dialog dialog){
        if(dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        }
    }

    protected void setTransparent(Dialog dialog){
        if(dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount(0);
        }
    }

    protected void showStatusBarWithouDim(final Dialog dialog){
        if(dialog != null) {
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Utils.showStatusBarWithouDim(getActivity(), ((Dialog)dialog));
                }
            });
        }
    }
}
