package com.silvioapps.malvelapp.adapters;

import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.malvelapp.interfaces.IViewClickListener;

public class Common {
    public static void setClickListeners(View view, final IViewClickListener viewClickListener){
        for (int index = 0; index < ((ViewGroup) view).getChildCount(); ++index) {
            final View nextChild = ((ViewGroup) view).getChildAt(index);
            nextChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (viewClickListener != null) {
                        viewClickListener.onClick(nextChild, (Integer) nextChild.getTag());
                    }
                }
            });

            if(nextChild instanceof ViewGroup) {
                setClickListeners(nextChild, viewClickListener);
            }
        }
    }

    public static void setTags(int position, View view){
        for (int index = 0; index < ((ViewGroup) view).getChildCount(); ++index) {
            final View nextChild = ((ViewGroup) view).getChildAt(index);
            nextChild.setTag(position);

            if(nextChild instanceof ViewGroup) {
                setTags(position, nextChild);
            }
        }
    }
}
