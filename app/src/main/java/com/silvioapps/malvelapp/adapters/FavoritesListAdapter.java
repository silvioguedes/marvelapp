package com.silvioapps.malvelapp.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.malvelapp.BR;
import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.databinding.CharactersListBaseObservable;
import com.silvioapps.malvelapp.interfaces.IViewClickListener;

import java.util.List;

public class FavoritesListAdapter extends RecyclerView.Adapter<FavoritesListAdapter.BindingViewHolder>{
    private List<CharactersListBaseObservable> listBaseObservableList = null;
    private static IViewClickListener viewClickListener = null;
    private BindingViewHolder viewHolder = null;

    public FavoritesListAdapter(List<CharactersListBaseObservable> listBaseObservableList, IViewClickListener viewClickListener_){
        this.listBaseObservableList = listBaseObservableList;
        viewClickListener = viewClickListener_;
    }

    public static class BindingViewHolder extends RecyclerView.ViewHolder{
        private ViewDataBinding viewDataBinding = null;

        public BindingViewHolder(View view) {
            super(view);

            viewDataBinding = DataBindingUtil.bind(view);

            Common.setClickListeners(view, viewClickListener);
        }

        public ViewDataBinding getBinding() {
            return viewDataBinding;
        }
    }

    @Override
    public BindingViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorites_list_layout, parent, false);
        viewHolder = new BindingViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final BindingViewHolder holder, final int position) {
        if(listBaseObservableList != null) {
            CharactersListBaseObservable listBaseObservable = listBaseObservableList.get(position);
            holder.getBinding().setVariable(BR.list, listBaseObservable);
            holder.getBinding().executePendingBindings();

            Common.setTags(position, holder.itemView);
        }
    }

    @Override
    public int getItemCount(){
        return listBaseObservableList == null ? 0 : listBaseObservableList.size();
    }
}
