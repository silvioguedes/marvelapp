package com.silvioapps.malvelapp.adapters;

import android.databinding.BindingAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.malvelapp.utils.Utils;

public class SizeBindingAdapter {
    @BindingAdapter("widthPerc")
    public static void setWidthPerc(View view, float widthPerc) {
        if(view.getContext() != null) {
            int displayWidth = Utils.getScreenSize(view.getContext()).x;
            int width = (int) (displayWidth * widthPerc) / 100;

            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            layoutParams.width = width;
        }
    }

    @BindingAdapter("heightPerc")
    public static void setHeightPerc(View view, float heightPerc) {
        if(view.getContext() != null) {
            int statusBarHeight = 0;
            if (!Utils.isAfterKitkat()) {
                statusBarHeight = Utils.getStatusBarHeight(view.getContext());
            }

            int displayHeight = Utils.getScreenSize(view.getContext()).y - statusBarHeight;
            int height = (int) (displayHeight * heightPerc) / 100;

            ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            layoutParams.height = height;
        }
    }
}
