package com.silvioapps.malvelapp.adapters;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.utils.Utils;
import com.squareup.picasso.Picasso;

public class ImageBindingAdapter {
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView imageView, String url) {
        if(imageView.getContext() != null) {
            Picasso.with(imageView.getContext())
                    .load(Utils.certifyNotNullOrEmptyUrl(url))
                    .placeholder(R.drawable.loading_image)
                    .into(imageView);
        }
    }
}
