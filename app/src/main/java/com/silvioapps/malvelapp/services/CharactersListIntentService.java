package com.silvioapps.malvelapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.silvioapps.malvelapp.constants.Constants;
import com.silvioapps.malvelapp.database.DatabaseHandler;
import com.silvioapps.malvelapp.models.RealmListStringTypeAdapter;
import com.silvioapps.malvelapp.models.RealmString;
import com.silvioapps.malvelapp.models.characters.Characters;
import com.silvioapps.malvelapp.receivers.MessageEvent;
import com.silvioapps.malvelapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.parceler.Parcels;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;

import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.silvioapps.malvelapp.R;

public class CharactersListIntentService extends IntentService {
    private Gson gson = null;

    public CharactersListIntentService() {
        super(CharactersListIntentService.class.getSimpleName());
    }

    @Override
    public void onCreate(){
        super.onCreate();

        gson = new GsonBuilder()
                .registerTypeAdapter(new TypeToken<RealmList<RealmString>>() {}.getType(), new RealmListStringTypeAdapter())
                .setLenient()
                .create();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if(intent != null) {
            if(intent.hasExtra(Constants.CHARACTERS_LIST)) {
                get(intent);

                intent.removeExtra(Constants.CHARACTERS_LIST);
            }
        }

        return START_NOT_STICKY;
    }

    protected void get(Intent intent){
        final Boolean response = intent.getBooleanExtra(Constants.CHARACTERS_LIST, false);
        final int limit = intent.getIntExtra(Constants.LIMIT, Constants.LIMIT_TOTAL);
        final int offset = intent.getIntExtra(Constants.OFFSET, 0);
        final long timestamp = intent.getLongExtra(Constants.TIMESTAMP, 0);
        final String apiKey = intent.getStringExtra(Constants.API_KEY);
        final String hash = intent.getStringExtra(Constants.HASH);

        if(Constants.OFFLINE_TEST){
            if(gson != null && response){
                try {
                    AssetManager assetManager = getAssets();
                    InputStream inputStream = assetManager.open(Constants.CHARACTERS_LIST_JSON_PATH);

                    Reader reader = new InputStreamReader(inputStream);

                    Type type = new TypeToken<Characters>(){}.getType();
                    Characters obj = gson.fromJson(reader, type);

                    returnMessage(obj);
                }catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
        else{
            if(Utils.isConnectionAvailable(getApplicationContext())){
                if(gson != null && response) {
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BASE_URL)
                            .addConverterFactory(GsonConverterFactory.create(gson))
                            .build();

                    Characters.API service = retrofit.create(Characters.API.class);
                    Call<Characters> call = service.get(limit, offset, timestamp, apiKey, hash);
                    call.enqueue(new Callback<Characters>() {
                        @Override
                        public void onResponse(Call<Characters> call, retrofit2.Response<Characters> response) {
                            final Characters obj = response.body();

                            if (response.isSuccessful()) {
                               DatabaseHandler.saveCharacters(obj);
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.server_error, Toast.LENGTH_SHORT).show();
                            }

                            returnMessage(obj);
                        }

                        @Override
                        public void onFailure(Call<Characters> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), R.string.server_error, Toast.LENGTH_SHORT).show();
                            returnMessage(null);
                        }
                    });
                }
            }
            else{
                returnMessage(null);

                Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        if(intent != null) {
            //TODO
        }

        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent){
        if(intent != null) {
            //TODO
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }

    protected void returnMessage(Characters obj){
        Intent intent = null;
        if(obj != null) {
            intent = new Intent(Constants.BROADCAST_ACTION);
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.CHARACTERS_LIST_PARCELABLE, Parcels.wrap(obj));
            intent.putExtra(Constants.CHARACTERS_LIST, bundle);
        }
        else{
            intent = new Intent(Constants.BROADCAST_ACTION);
            intent.putExtra(Constants.CHARACTERS_LIST, (Parcelable)null);
        }

        EventBus.getDefault().postSticky(new MessageEvent(intent));
    }
}
