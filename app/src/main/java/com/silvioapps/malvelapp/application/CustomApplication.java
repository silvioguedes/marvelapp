package com.silvioapps.malvelapp.application;

import android.app.Application;

import com.silvioapps.malvelapp.constants.Constants;

import io.realm.Realm;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import com.silvioapps.malvelapp.R;

public class CustomApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(Constants.FONT_NORMAL_PATH)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}

