package com.silvioapps.malvelapp.receivers;

import android.content.Intent;

public class MessageEvent{
    private Intent intent = null;

    public MessageEvent(Intent intent) {
        this.intent = intent;
    }

    public Intent getIntent() {
        return intent;
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
