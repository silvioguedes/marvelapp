package com.silvioapps.malvelapp.database;

import com.silvioapps.malvelapp.models.characters.Characters;
import com.silvioapps.malvelapp.models.characters.Comics;
import com.silvioapps.malvelapp.models.characters.Data;
import com.silvioapps.malvelapp.models.characters.Events;
import com.silvioapps.malvelapp.models.characters.ItemsItem;
import com.silvioapps.malvelapp.models.characters.ResultsItem;
import com.silvioapps.malvelapp.models.characters.Series;
import com.silvioapps.malvelapp.models.characters.Stories;
import com.silvioapps.malvelapp.models.characters.Thumbnail;
import com.silvioapps.malvelapp.models.characters.UrlsItem;

import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.exceptions.RealmMigrationNeededException;

public class DatabaseHandler {
    private static DatabaseHandler instance = null;
    private static Realm realm = null;

    private DatabaseHandler(){}

    public static DatabaseHandler getInstance(){
        if(instance == null){
            instance = new DatabaseHandler();
        }

        return instance;
    }

    public static void destroyInstance(){
        instance = null;
    }

    private static Realm getRealm(){
        RealmConfiguration realmConfiguration = new RealmConfiguration
                .Builder()
                .build();

        try {
            realm = Realm.getInstance(realmConfiguration);
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(realmConfiguration);
                realm = Realm.getInstance(realmConfiguration);
            } catch (Exception ex){
                throw ex;
            }
        }

        return realm;
    }

    public static void closeRealm(){
        if(realm != null) {
            try {
                if(!realm.isClosed()) {
                    realm.close();
                }
            }
            catch(IllegalStateException e){
                e.printStackTrace();
            }

            realm = null;
        }
    }

    public static boolean isValid(RealmObject realmObject){
        return realmObject.isValid();
    }

    public static boolean isValid(List<RealmObject> realmObjectList){
        for(RealmObject realmObject : realmObjectList){
            if(!realmObject.isValid()){
                return false;
            }
        }

        return true;
    }

    //********************************************

    public static void saveCharacters(final Characters obj){
        Realm realm = getRealm();

        if(obj != null) {
            try{
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        Characters characters = realm.createObject(Characters.class);
                        characters.setAttributionHTML(obj.getAttributionHTML());
                        characters.setAttributionText(obj.getAttributionText());
                        characters.setCode(obj.getCode());
                        characters.setCopyright(obj.getCopyright());

                        Data data = realm.createObject(Data.class);
                        data.setCount(obj.getData().getCount());
                        data.setLimit(obj.getData().getLimit());
                        data.setOffset(obj.getData().getOffset());

                        RealmList<ResultsItem> resultsItemRealmList = new RealmList<>();
                        for(ResultsItem obj2 : obj.getData().getResults()){
                            ResultsItem resultsItem = realm.createObject(ResultsItem.class);

                            Comics comics = realm.createObject(Comics.class);
                            comics.setAvailable(obj2.getComics().getAvailable());
                            comics.setCollectionURI(obj2.getComics().getCollectionURI());

                            RealmList<ItemsItem> itemsItemRealmList = new RealmList<>();
                            for(ItemsItem obj3 : obj2.getComics().getItems()){
                                ItemsItem itemsItem = realm.createObject(ItemsItem.class);
                                itemsItem.setName(obj3.getName());
                                itemsItem.setResourceURI(obj3.getResourceURI());
                                itemsItem.setType(obj3.getType());

                                itemsItemRealmList.add(itemsItem);
                            }

                            comics.setItems(itemsItemRealmList);
                            comics.setReturned(obj2.getComics().getReturned());

                            resultsItem.setComics(comics);
                            resultsItem.setDescription(obj2.getDescription());

                            Events events = realm.createObject(Events.class);
                            events.setAvailable(obj2.getEvents().getAvailable());
                            events.setCollectionURI(obj2.getEvents().getCollectionURI());

                            RealmList<ItemsItem> itemsItemRealmList2 = new RealmList<>();
                            for(ItemsItem obj3 : obj2.getEvents().getItems()){
                                ItemsItem itemsItem = realm.createObject(ItemsItem.class);
                                itemsItem.setName(obj3.getName());
                                itemsItem.setResourceURI(obj3.getResourceURI());
                                itemsItem.setType(obj3.getType());

                                itemsItemRealmList2.add(itemsItem);
                            }

                            events.setItems(itemsItemRealmList2);
                            events.setReturned(obj2.getEvents().getReturned());

                            resultsItem.setEvents(events);
                            resultsItem.setId(obj2.getId());
                            resultsItem.setModified(obj2.getModified());
                            resultsItem.setName(obj2.getName());
                            resultsItem.setResourceURI(obj2.getResourceURI());

                            Series series = realm.createObject(Series.class);
                            series.setAvailable(obj2.getSeries().getAvailable());
                            series.setCollectionURI(obj2.getSeries().getCollectionURI());

                            RealmList<ItemsItem> itemsItemRealmList3 = new RealmList<>();
                            for(ItemsItem obj3 : obj2.getSeries().getItems()){
                                ItemsItem itemsItem = realm.createObject(ItemsItem.class);
                                itemsItem.setName(obj3.getName());
                                itemsItem.setResourceURI(obj3.getResourceURI());
                                itemsItem.setType(obj3.getType());

                                itemsItemRealmList3.add(itemsItem);
                            }

                            series.setItems(itemsItemRealmList3);
                            series.setReturned(obj2.getSeries().getReturned());

                            resultsItem.setSeries(series);

                            Stories stories = realm.createObject(Stories.class);
                            stories.setAvailable(obj2.getStories().getAvailable());
                            stories.setCollectionURI(obj2.getStories().getCollectionURI());

                            RealmList<ItemsItem> itemsItemRealmList4 = new RealmList<>();
                            for(ItemsItem obj3 : obj2.getStories().getItems()){
                                ItemsItem itemsItem = realm.createObject(ItemsItem.class);
                                itemsItem.setName(obj3.getName());
                                itemsItem.setResourceURI(obj3.getResourceURI());
                                itemsItem.setType(obj3.getType());

                                itemsItemRealmList4.add(itemsItem);
                            }

                            stories.setItems(itemsItemRealmList4);
                            stories.setReturned(obj2.getStories().getReturned());

                            resultsItem.setStories(stories);

                            Thumbnail thumbnail = realm.createObject(Thumbnail.class);
                            thumbnail.setExtension(obj2.getThumbnail().getExtension());
                            thumbnail.setPath(obj2.getThumbnail().getPath());

                            resultsItem.setThumbnail(thumbnail);

                            RealmList<UrlsItem> urlsItemRealmList = new RealmList<>();
                            for(UrlsItem obj4 : obj2.getUrls()){
                                UrlsItem urlsItem = realm.createObject(UrlsItem.class);
                                urlsItem.setType(obj4.getType());
                                urlsItem.setUrl(obj4.getUrl());

                                urlsItemRealmList.add(urlsItem);
                            }

                            resultsItem.setUrls(urlsItemRealmList);
                            resultsItem.setFavorited(false);

                            resultsItemRealmList.add(resultsItem);
                        }

                        data.setResults(resultsItemRealmList);
                        data.setTotal(obj.getData().getTotal());

                        characters.setData(data);
                        characters.setEtag(obj.getEtag());
                        characters.setStatus(obj.getStatus());
                    }
                });
            }
            catch(Exception e){
                e.printStackTrace();
            }
            finally{
                closeRealm();
            }
        }
    }

    public static Characters getCharacters(){
        Realm realm = getRealm();

        Characters obj = null;
        try {
            realm.beginTransaction();

            obj = realm.where(Characters.class).findFirst();

            realm.commitTransaction();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return obj;
    }

    public static List<ResultsItem> getResultsItems(){
        Realm realm = getRealm();

        List<ResultsItem> obj = null;
        try {
            realm.beginTransaction();

            obj = new ArrayList<>(realm.where(ResultsItem.class).findAll());

            realm.commitTransaction();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return obj;
    }

    public static ResultsItem getResultsItem(int id){
        Realm realm = getRealm();

        ResultsItem obj = null;
        try {
            realm.beginTransaction();

            obj = realm.where(ResultsItem.class).equalTo("id",id).findFirst();

            realm.commitTransaction();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return obj;
    }

    public static void saveFavorite(int id, boolean favorite) {
        Realm realm = getRealm();

        try {
            realm.beginTransaction();

            ResultsItem resultsItem = realm.where(ResultsItem.class).equalTo("id",id).findFirst();
            if(resultsItem != null) {
                resultsItem.setFavorited(favorite);
            }

            realm.commitTransaction();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static List<ResultsItem> getFavorites(){
        Realm realm = getRealm();

        List<ResultsItem> obj = null;
        try {
            realm.beginTransaction();

            obj = new ArrayList<>(realm.where(ResultsItem.class).equalTo("favorited",true).findAll());

            realm.commitTransaction();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return obj;
    }
}
