package com.silvioapps.malvelapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.malvelapp.interfaces.IViewClickListener;
import com.silvioapps.malvelapp.receivers.MessageEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class CustomFragment extends Fragment implements IViewClickListener {

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        return null;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
     }

    @Override
    public void onDestroy(){
        super.onDestroy();

        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            //todo
        }
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void onClick(View view, int position) {}

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if(event != null && event.getIntent() != null) {
            onReceiveMessage(event.getIntent());
            EventBus.getDefault().removeStickyEvent(event);
        }
    }

    public void onReceiveMessage(Intent intent){}
}
