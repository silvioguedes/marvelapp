package com.silvioapps.malvelapp.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.activities.CharacterInfoActivity;
import com.silvioapps.malvelapp.adapters.FavoritesListAdapter;
import com.silvioapps.malvelapp.constants.Constants;
import com.silvioapps.malvelapp.database.DatabaseHandler;
import com.silvioapps.malvelapp.databinding.CharactersListBaseObservable;
import com.silvioapps.malvelapp.models.characters.ResultsItem;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;

public class FavoritesListFragment extends CustomFragment{
    private FavoritesListAdapter listAdapter = null;
    private List<CharactersListBaseObservable> listBaseObservableList = null;
    private List<ResultsItem> list = null;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, final Bundle bundle){
        final View view = layoutInflater.inflate(R.layout.fragment_characters_list, viewGroup, false);

        List<ResultsItem> resultsItemList = DatabaseHandler.getFavorites();
        if (bundle == null && getActivity() != null) {
            getData();
        }

        if(getActivity() != null) {
            listAdapter = new FavoritesListAdapter(listBaseObservableList, this);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

            final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(listAdapter);
        }

        if(bundle == null){
            if(resultsItemList != null && DatabaseHandler.isValid(new ArrayList<RealmObject>(resultsItemList)) &&
                    resultsItemList.size() > 0) {
                showList(resultsItemList);
            }
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        DatabaseHandler.closeRealm();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            //todo
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void onClick(View view, int position) {
        showCharacterInfoActivity(position);
    }

    @Override
    public void onReceiveMessage(Intent intent) {
        if (intent.hasExtra(Constants.CHARACTER_INFO)) {
            if (intent.getBundleExtra(Constants.CHARACTER_INFO) != null) {
                Parcelable parcelable = intent.getBundleExtra(Constants.CHARACTER_INFO).getParcelable(Constants.CHARACTER_INFO_PARCELABLE);
                if (parcelable != null) {
                    CharactersListBaseObservable charactersListBaseObservable = Parcels.unwrap(parcelable);

                    if(charactersListBaseObservable != null) {
                        updateList(charactersListBaseObservable);
                    }
                }
            }

            intent.removeExtra(Constants.CHARACTERS_LIST);
        }
    }

    protected void getData(){
        listBaseObservableList = new ArrayList<>();
        listBaseObservableList.clear();
    }

    protected void showList(List<ResultsItem> obj){
        list = new ArrayList<>();
        list.clear();
        list.addAll(obj);

        fillList(list);
    }

    protected void fillList(List<ResultsItem> list){
        if(list == null || list.size() == 0){
            Toast.makeText(getActivity(),R.string.no_results,Toast.LENGTH_SHORT).show();
        }

        if (list != null && listBaseObservableList != null) {
            for (ResultsItem obj : list) {
                int id = obj.getId();
                String name = obj.getName();
                String description = obj.getDescription();
                String image = obj.getThumbnail().getPath() + "." + obj.getThumbnail().getExtension();

                CharactersListBaseObservable characterListBaseObservable = new CharactersListBaseObservable(id, name, description, image);
                characterListBaseObservable.setFavorited(obj.isFavorited());

                listBaseObservableList.add(characterListBaseObservable);
            }

            if(listAdapter != null) {
                listAdapter.notifyDataSetChanged();
            }
        }
    }

    protected void updateList(CharactersListBaseObservable charactersListBaseObservable){
        if(listBaseObservableList != null) {
            for (CharactersListBaseObservable obj : listBaseObservableList) {
                if(obj != null && obj.getId() == charactersListBaseObservable.getId()){
                    obj.setFavorited(charactersListBaseObservable.isFavorited());

                    if(!charactersListBaseObservable.isFavorited()){
                        listBaseObservableList.remove(obj);
                    }
                }
            }

            if(listAdapter != null) {
                listAdapter.notifyDataSetChanged();
            }
        }
    }

    protected void showCharacterInfoActivity(int position){
        if(listBaseObservableList != null && listBaseObservableList.size() > position) {
            CharactersListBaseObservable obj = listBaseObservableList.get(position);

            String description = obj.getDescription();
            if(description == null || description.isEmpty()){
                description = getString(R.string.no_description);
            }

            obj.setDescription(description);

            ResultsItem resultsItem = DatabaseHandler.getResultsItem(obj.getId());
            if(resultsItem != null) {
                obj.setFavorited(resultsItem.isFavorited());

                Intent intent = new Intent(getActivity(), CharacterInfoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.CHARACTER_INFO_PARCELABLE, Parcels.wrap(obj));
                intent.putExtra(Constants.CHARACTER_INFO, bundle);
                getActivity().startActivity(intent);
            }

            DatabaseHandler.closeRealm();
        }
    }
}
