package com.silvioapps.malvelapp.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.constants.Constants;
import com.silvioapps.malvelapp.databinding.CharactersListBaseObservable;
import com.silvioapps.malvelapp.databinding.FragmentCharacterInfoBinding;
import com.silvioapps.malvelapp.utils.Utils;

import org.parceler.Parcels;

public class CharacterInfoFragment extends CustomFragment{

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = null;

        if(getArguments() != null && getActivity() != null) {
            Parcelable parcelable = getArguments().getParcelable(Constants.CHARACTER_INFO_PARCELABLE);
            if (parcelable != null) {
                CharactersListBaseObservable characterListBaseObservable = Parcels.unwrap(parcelable);

                FragmentCharacterInfoBinding fragmentCharacterInfoBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_character_info, viewGroup, false);
                fragmentCharacterInfoBinding.setList(characterListBaseObservable);

                view = fragmentCharacterInfoBinding.getRoot();
                Utils.setMargin(view, 0 , Utils.getStatusBarHeight(getActivity()), 0, 0);
            }
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            //todo
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }
}
