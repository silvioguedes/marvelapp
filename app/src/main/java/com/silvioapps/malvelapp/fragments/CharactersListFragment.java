package com.silvioapps.malvelapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.silvioapps.malvelapp.R;
import com.silvioapps.malvelapp.activities.CharacterInfoActivity;
import com.silvioapps.malvelapp.adapters.CharactersListAdapter;
import com.silvioapps.malvelapp.constants.Constants;
import com.silvioapps.malvelapp.database.DatabaseHandler;
import com.silvioapps.malvelapp.databinding.CharactersListBaseObservable;
import com.silvioapps.malvelapp.dialogs.LoadingDialogFragment;
import com.silvioapps.malvelapp.models.characters.Characters;
import com.silvioapps.malvelapp.models.characters.ResultsItem;
import com.silvioapps.malvelapp.services.CharactersListIntentService;
import com.silvioapps.malvelapp.statics.Statics;
import com.silvioapps.malvelapp.utils.Utils;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;

public class CharactersListFragment extends CustomFragment{
    private CharactersListAdapter listAdapter = null;
    private List<CharactersListBaseObservable> listBaseObservableList = null;
    private List<ResultsItem> list = null;
    private int offset = 0;
    private CharactersListBaseObservable charactersListBaseObservable = null;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle bundle){
        super.onCreate(bundle);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle){
        View view = layoutInflater.inflate(R.layout.fragment_characters_list, viewGroup, false);

        List<ResultsItem> resultsItemList = DatabaseHandler.getResultsItems();
        if (bundle == null && getActivity() != null) {
            getData(getActivity(),resultsItemList);
        }

        if(getActivity() != null) {
            listAdapter = new CharactersListAdapter(listBaseObservableList, this);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());

            final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(listAdapter);
            recyclerView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (!Statics.loadMore && !recyclerView.canScrollVertically(1) && event != null &&
                            event.getAction() == MotionEvent.ACTION_UP) {

                        Statics.loadMore = true;
                        loadMore();
                    }

                    return false;
                }
            });
        }

        if(bundle == null){
            if(resultsItemList != null && DatabaseHandler.isValid(new ArrayList<RealmObject>(resultsItemList)) &&
                    resultsItemList.size() > 0) {
                showList(resultsItemList);
            }
        }

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onStop(){
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        DatabaseHandler.closeRealm();
    }

    @Override
    public void onDestroy(){
        if(getActivity() != null && !getActivity().isChangingConfigurations()) {
            //todo
        }

        super.onDestroy();
    }

    @Override
    public void onDetach(){
        super.onDetach();
    }

    @Override
    public void onClick(View view, int position) {
        showCharacterInfoActivity(position);
    }

    @Override
    public void onReceiveMessage(Intent intent) {
        if (intent.hasExtra(Constants.CHARACTERS_LIST)) {
            LoadingDialogFragment.getInstance().cancel();
            LoadingDialogFragment.destroyInstance();

            if (intent.getBundleExtra(Constants.CHARACTERS_LIST) != null) {
                Parcelable parcelable = intent.getBundleExtra(Constants.CHARACTERS_LIST).getParcelable(Constants.CHARACTERS_LIST_PARCELABLE);
                if (parcelable != null) {
                    Characters characters = Parcels.unwrap(parcelable);

                    if(characters != null && characters.getData() != null && characters.getData().getResults() != null) {
                        showList(characters.getData().getResults());
                    }
                }
            }

            intent.removeExtra(Constants.CHARACTERS_LIST);
        }
    }

    protected void getData(Activity activity, List<ResultsItem> resultsItemList){
        if (activity != null) {
            if(resultsItemList == null || !DatabaseHandler.isValid(new ArrayList<RealmObject>(resultsItemList)) ||
                    resultsItemList.size() <= 0) {
                LoadingDialogFragment.getInstance().showLoadingFragment(this);

                offset = 0;
                startService(activity, offset);
            }

            listBaseObservableList = new ArrayList<>();
            listBaseObservableList.clear();
        }
    }

    protected void startService(Activity activity, int offset){
        if(getActivity() != null) {
            long timestamp = System.currentTimeMillis();
            String apiKey = Constants.API_KEY_VALUE;
            String hash = Utils.MD5(timestamp+Constants.PRIVATE_KEY_VALUE+apiKey);

            Intent newIntent = new Intent(activity, CharactersListIntentService.class);
            newIntent.putExtra(Constants.CHARACTERS_LIST, true);
            newIntent.putExtra(Constants.LIMIT, Constants.LIMIT_TOTAL);
            newIntent.putExtra(Constants.OFFSET, offset);
            newIntent.putExtra(Constants.TIMESTAMP, timestamp);
            newIntent.putExtra(Constants.API_KEY, apiKey);
            newIntent.putExtra(Constants.HASH, hash);
            activity.startService(newIntent);
        }
    }

    protected void showList(List<ResultsItem> obj){
        list = new ArrayList<>();
        list.clear();
        list.addAll(obj);

        fillList(list);
    }

    protected void fillList(List<ResultsItem> list){
        if(list == null || list.size() == 0){
            Toast.makeText(getActivity(),R.string.no_results,Toast.LENGTH_SHORT).show();
        }

        if (list != null && listBaseObservableList != null) {
            for (ResultsItem obj : list) {
                int id = obj.getId();
                String name = obj.getName();
                String description = obj.getDescription();
                String image = obj.getThumbnail().getPath()+"."+obj.getThumbnail().getExtension();

                CharactersListBaseObservable characterListBaseObservable = new CharactersListBaseObservable(id, name, description, image);
                characterListBaseObservable.setFavorited(obj.isFavorited());

                listBaseObservableList.add(characterListBaseObservable);
            }

            if(charactersListBaseObservable != null){
                charactersListBaseObservable.setShowLoading(false);
                listBaseObservableList.remove(charactersListBaseObservable);
                Statics.loadMore = false;
            }

            if(listAdapter != null) {
                listAdapter.notifyDataSetChanged();
            }

            offset = listBaseObservableList.size();
        }
    }

    protected void loadMore(){
        if(listBaseObservableList != null){
            charactersListBaseObservable = new CharactersListBaseObservable();
            charactersListBaseObservable.setShowLoading(true);

            listBaseObservableList.add(charactersListBaseObservable);

            if(listAdapter != null) {
                listAdapter.notifyDataSetChanged();
            }
        }

        offset = offset + Constants.LIMIT_TOTAL;
        startService(getActivity(), offset);
    }

    protected void showCharacterInfoActivity(int position){
        if(listBaseObservableList != null && listBaseObservableList.size() > position) {
            CharactersListBaseObservable obj = listBaseObservableList.get(position);

            String description = obj.getDescription();
            if(description == null || description.isEmpty()){
                description = getString(R.string.no_description);
            }

            obj.setDescription(description);

            ResultsItem resultsItem = DatabaseHandler.getResultsItem(obj.getId());
            if(resultsItem != null) {
                obj.setFavorited(resultsItem.isFavorited());

                Intent intent = new Intent(getActivity(), CharacterInfoActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.CHARACTER_INFO_PARCELABLE, Parcels.wrap(obj));
                intent.putExtra(Constants.CHARACTER_INFO, bundle);
                getActivity().startActivity(intent);
            }

            DatabaseHandler.closeRealm();
        }
    }
}
