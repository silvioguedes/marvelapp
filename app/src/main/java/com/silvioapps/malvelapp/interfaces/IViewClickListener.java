package com.silvioapps.malvelapp.interfaces;

import android.view.View;

public interface IViewClickListener {
    void onClick(View view, int position);
}
