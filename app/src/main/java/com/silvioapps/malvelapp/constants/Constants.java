package com.silvioapps.malvelapp.constants;

public class Constants {
    public static final boolean OFFLINE_TEST = false;

    public static final String SHARED_PREFERENCES = "SHARED_PREFERENCES";
    public static final long SPLASH_SCREEN_SLEEP = 2500;

    //https://gateway.marvel.com/v1/public/characters?offset=0&ts=1497998150&apikey=d7449a1a243930e24c8dd553afa44032&hash=8d33e21c4bd854cedfdc03e9ab60ab1b
    public static final String BASE_URL = "https://gateway.marvel.com/v1/public/";
    public static final String CHARACTERS_LIST_URL = "characters?";
    public static final String CHARACTERS_LIST_JSON_PATH = "jsons/characters_list_json.txt";

    public static final String CHARACTERS_LIST = "CHARACTERS_LIST";
    public static final String CHARACTERS_LIST_PARCELABLE = "CHARACTERS_LIST_PARCELABLE";
    public static final String BROADCAST_ACTION = "BROADCAST_ACTION";
    public static final String API_KEY = "API_KEY";
    public static final String OFFSET = "OFFSET";
    public static final String TIMESTAMP = "TIMESTAMP";
    public static final String HASH = "HASH";
    public static final String API_KEY_VALUE = "d7449a1a243930e24c8dd553afa44032";
    public static final String PRIVATE_KEY_VALUE = "f53c4642066307893d2906941663d043f96caefe";
    public static final String CHARACTER_INFO = "CHARACTER_INFO";
    public static final String CHARACTER_INFO_PARCELABLE = "CHARACTER_INFO_PARCELABLE";
    public static final String LIMIT = "LIMIT";

    public static final int ROUNDED_IMAGE_SIZE_PERCENT = 5;
    public static final int LIMIT_TOTAL = 20;

    public static final String FONT_NORMAL_PATH = "fonts/Roboto-Regular.ttf";
}
